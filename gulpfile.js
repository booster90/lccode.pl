const { src, dest, series, watch } = require('gulp');
const terser = require('gulp-terser');
const rename = require('gulp-rename');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const concatCss = require('gulp-concat-css');
const sass = require('gulp-sass');

/**
 * minimalizacja plików js
 * @param {*} cb 
 */
function buildJs(cb) {
    console.log('build javascript files..')

    return src('src/js/*.js')
        .pipe(concat('bundle.js'))
        .pipe(terser())
        .pipe(rename({ extname: '.min.js' }))
        .pipe(dest('assets/'));
}

/**
 * Minimalizacja i budowanie css z plikow sass
 * @param {*} cb 
 */
function buildSass(cb) {
    console.log('build sass files..')

    return src('src/scss/*.scss')
        // zamienic sass na css
        .pipe(sass())

        // połącz pliki
        .pipe(concatCss('bundle.min.css'))
        
        // minimalizacja css
        .pipe(cleanCSS({debug: true}, (details) => {
            console.log(`${details.name}: ${details.stats.originalSize}`);
            console.log(`${details.name}: ${details.stats.minifiedSize}`);
        }))
        .pipe(dest('assets/'));
}

exports.default = function() {
    // zbuduj pliki css z scss i zapisz w katalogu assets
    watch('src/scss/**.scss', {ignoreInitial: false}, series(buildSass));

    // sprawdzaj pliki js
    watch('src/js/**.js', {ignoreInitial: false}, series(buildJs));
}

exports.build = series(buildSass, buildJs);
